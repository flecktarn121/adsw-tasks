package com.miw.presentation.user;

import org.apache.logging.log4j.*;

import com.miw.infrastructure.Factories;
import com.miw.model.User;

public class UserManagerServiceHelper {
	Logger logger = LogManager.getLogger(this.getClass());

	public User getUser(String username) throws Exception {
		logger.debug("Retrieving a user from Business Layer");
		return (Factories.services.getUserManagerService()).getUser(username);
	}

	public void addUser(User user) throws Exception {
		logger.debug("Retrieving a user from Business Layer");
		(Factories.services.getUserManagerService()).addUser(user);
	}

	public boolean existsUser(String username) throws Exception {
		logger.debug("Checking existance of user from Business Layer");
		return (Factories.services.getUserManagerService()).existsUser(username);
	}

}
