package com.miw.presentation.vat;

import java.util.List;

import org.apache.logging.log4j.*;
import com.miw.infrastructure.Factories;
import com.miw.model.VAT;

public class VATManagerServiceHelper {

	Logger logger = LogManager.getLogger(this.getClass());

	public List<VAT> getVATs() throws Exception {
		logger.debug("Retrieving VATs from Business Layer");
		return (Factories.services.getVATManagerService()).getVATs();
	}
}
