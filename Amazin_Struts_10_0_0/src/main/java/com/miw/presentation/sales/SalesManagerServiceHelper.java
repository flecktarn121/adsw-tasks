package com.miw.presentation.sales;

import org.apache.logging.log4j.*;

import com.miw.infrastructure.Factories;
import com.miw.model.ShoppingCart;

public class SalesManagerServiceHelper {
	Logger logger = LogManager.getLogger(this.getClass());
	
	
	public void processCart(ShoppingCart cart) throws Exception {
		logger.debug("Processing shopping cart");
		(Factories.services.getSalesManagerService()).processCart(cart);
	}
}
