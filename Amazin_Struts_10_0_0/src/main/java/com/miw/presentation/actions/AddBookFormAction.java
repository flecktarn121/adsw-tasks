package com.miw.presentation.actions;

import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.LoginInfo;
import com.miw.model.User;
import com.miw.presentation.user.UserManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@Results({ @Result(name = "success", location = "add-books.jsp"),
		@Result(name = "non-admin-error", location = "login-success.jsp")
})
public class AddBookFormAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = -1486407945105490539L;
	private Map<String, Object> session;
	
	@Override
	public String execute() throws Exception {
		LoginInfo login = (LoginInfo) session.get("loginInfo");
		User user = (new UserManagerServiceHelper()).getUser(login.getLogin());
		
		if(!user.getIsAdmin()) {
			return "non-admin-error";
		}
		return SUCCESS;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
