package com.miw.presentation.actions;

import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

import org.apache.logging.log4j.*;
import com.miw.business.exception.AlreadyRegisteredUserException;
import com.miw.model.LoginInfo;
import com.miw.model.SignupInfo;
import com.miw.model.User;
import com.miw.presentation.user.UserManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

@Results({ @Result(name = "success", location = "login-success.jsp"),
		@Result(name = "already-registered-error", location = "/signup.jsp"),

		// For validation
		@Result(name = "input", location = "/signup.jsp")

})

@Validations(requiredStrings = {
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "signupInfo.username", message = "You must enter a value for username."),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "signupInfo.password", message = "You must enter a value for password.") }, stringLengthFields = {
				@StringLengthFieldValidator(type = ValidatorType.SIMPLE, trim = true, minLength = "8", maxLength = "128", fieldName = "signupInfo.username", message = "Username must be between 8 and 128 characters"),
				@StringLengthFieldValidator(type = ValidatorType.SIMPLE, trim = true, minLength = "8", maxLength = "128", fieldName = "signupInfo.password", message = "Password must be between 8 and 128 characters") })

public class SignupAction extends ActionSupport implements SessionAware, RequestAware {
	Logger logger = LogManager.getLogger(this.getClass());
	private Map<String, Object> session = null;
	private Map<String, Object> request = null;
	private SignupInfo signupInfo = null;

	@Override
	public String execute() throws Exception {
		UserManagerServiceHelper helper = new UserManagerServiceHelper();
		try {
			User user = createUser();
			helper.addUser(user);

			startSession(user);
			return SUCCESS;
		} catch (AlreadyRegisteredUserException e) {
			logger.debug("User already registered: " + signupInfo.getUsername());
			request.put("mymessage", "Username already registered!");
			return "already-registered-error";
		}
	}

	private User createUser() {
		User user = new User(signupInfo.getUsername(), signupInfo.getPassword(), false);
		return user;

	}

	private void startSession(User user) {
		LoginInfo login = new LoginInfo();
		login.setLogin(user.getUsername());
		login.setPassword(user.getPassword());
		login.setCaptcha("23344343");

		this.session.put("loginInfo", login);
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public void setSignupInfo(SignupInfo signupInfo) {
		this.signupInfo = signupInfo;
	}
	
	public SignupInfo getSignupInfo() {
		return this.signupInfo;
	}

}
