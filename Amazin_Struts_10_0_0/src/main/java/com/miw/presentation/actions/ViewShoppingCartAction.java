package com.miw.presentation.actions;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.Book;
import com.miw.model.BookCartItem;
import com.miw.model.ShoppingCart;
import com.miw.presentation.book.BookManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@Result(name="success", location="view-shopping-cart.jsp" )
public class ViewShoppingCartAction extends ActionSupport implements SessionAware, RequestAware, ApplicationAware{
	private static final long serialVersionUID = 5492115691163192547L;
	private Map<String, Object> session = null;
	private Map<String, Object> request = null;
	private Map<String, Object> application = null;
	
	@Override
	public String execute() throws Exception {
		double totalAmount = calculateTotalAmount();
		this.request.put("totalAmount", totalAmount);

		return SUCCESS;
	}
	
	public double calculateTotalAmount() throws Exception {
		ShoppingCart cart = getShoppingCart();
		List<Book> books = getBooks();
		double totalAmount = 0;
		
		for(BookCartItem book: cart.getList().values()) {
			totalAmount += book.getBook().getPrice() * book.getAmount();
		}
		
		return totalAmount;

	}

	public ShoppingCart getShoppingCart() {
		if (!this.session.containsKey("cart")) {
			this.session.put("cart", new ShoppingCart());
		}

		return (ShoppingCart) this.session.get("cart");
	}

	public List<Book> getBooks() throws Exception {
		if (!this.application.containsKey("booklist")) {
			this.application.put("booklist", (new BookManagerServiceHelper()).getBooks());
		}

		return (List<Book>) this.application.get("booklist");
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public void setApplication(Map<String, Object> application) {
		this.application = application;
	}

}
