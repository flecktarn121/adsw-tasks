package com.miw.presentation.actions;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ApplicationAware;

import com.miw.model.Book;
import com.miw.model.VAT;
import com.miw.presentation.book.BookManagerServiceHelper;
import com.miw.presentation.vat.VATManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

@Results({ @Result(name = "success", location = "show-books.action", type = "redirectAction"),
		// For validation
		@Result(name = "input", location = "add-books.jsp")

})

@Validations(requiredStrings = {
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "book.title", message = "You must enter a title."),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "book.description", message = "You must enter a description."),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "book.author", message = "You must enter an author."),
		}, 
stringLengthFields = {
				@StringLengthFieldValidator(type = ValidatorType.SIMPLE, trim = true, minLength = "8", maxLength = "128", fieldName = "book.title", message = "Title must be between 8 and 128 characters"),
				@StringLengthFieldValidator(type = ValidatorType.SIMPLE, trim = true, minLength = "8", maxLength = "256", fieldName = "book.description", message = "Description must be between 8 and 256 characters"),
				@StringLengthFieldValidator(type = ValidatorType.SIMPLE, trim = true, minLength = "8", maxLength = "128", fieldName = "book.author", message = "Title must be between 8 and 128 characters")
				})

public class AddBookAction extends ActionSupport implements ApplicationAware {
	private static final long serialVersionUID = 4494367035897573636L;
	private Map<String, Object> application;
	private Book book;
	private String vatText;
	private String stockText;
	private String basePriceText;

	@Override
	public String execute() throws Exception {

		if (!isValidVAT()) {
			addFieldError("vatText", "Select a valid VAT from the options available");
			return "input";
		}

		if (!isValidStock()) {
			addFieldError("stockText", "Stock must be an integer number above 1");
			return "input";
		}

		if (!isValidBasePrice()) {
			addFieldError("basePriceText", "Base price must be a number above 0 with two decimals");
			return "input";
		}
		
		(new BookManagerServiceHelper()).addBook(book);
		this.application.put("booklist", (new BookManagerServiceHelper()).getBooks());

		return SUCCESS;
	}

	private boolean isValidBasePrice() {
		try {
			double price = Double.parseDouble(basePriceText);
			if (price < 0.01) {
				return false;
			}
			book.setBasePrice(price);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private boolean isValidStock() {
		try {
			int stock = Integer.parseInt(stockText);
			if (stock < 1) {
				return false;
			}
			book.setStock(stock);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private boolean isValidVAT() throws Exception {
		try {
			if (vatText.equals("16%")) {
				VAT vat = getVATForTaxGroup(0);
				book.setVat(vat);
				return true;
			}
			if (vatText.equals("7%")) {
				VAT vat = getVATForTaxGroup(1);
				book.setVat(vat);
				return true;
			}
		} catch (IllegalArgumentException e) {
			return false;
		}
		return false;
	}

	private VAT getVATForTaxGroup(int taxGroup) throws Exception {
		List<VAT> vats = (new VATManagerServiceHelper()).getVATs();

		for (VAT vat : vats) {
			if (vat.getTaxGroup() == taxGroup) {
				return vat;
			}
		}

		throw new IllegalArgumentException();
	}

	public void setApplication(Map<String, Object> application) {
		this.application = application;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Book getBook() {
		return book;
	}

	public void setStockText(String stockText) {
		this.stockText = stockText;
	}

	public void setBasePriceText(String basePriceText) {
		this.basePriceText = basePriceText;
	}

	public void setVatText(String vatText) {
		this.vatText = vatText;
	}
}
