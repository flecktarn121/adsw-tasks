package com.miw.presentation.actions;

import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ApplicationAware;

import com.miw.presentation.book.BookManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@Result(name="success", location="add-to-shopping-cart.jsp" )
public class AddToShoppingCartFormAction extends ActionSupport implements ApplicationAware {

	private static final long serialVersionUID = 7300559233286950373L;

	private Map<String, Object> application = null;

	@Override
	public String execute() throws Exception {
		if (!this.application.containsKey("booklist")) {
			this.application.put("booklist", (new BookManagerServiceHelper()).getBooks());
		}

		return SUCCESS;
	}

	public void setApplication(Map<String, Object> application) {
		this.application = application;
	}

}
