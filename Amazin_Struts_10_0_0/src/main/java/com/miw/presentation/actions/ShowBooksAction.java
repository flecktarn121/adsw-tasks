package com.miw.presentation.actions;

import java.util.Map;

import org.apache.logging.log4j.*;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.LoginInfo;
import com.miw.presentation.book.BookManagerServiceHelper;
import com.miw.presentation.user.UserManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value ="miw.Amazin")

public class ShowBooksAction extends ActionSupport implements RequestAware, SessionAware  {
	
	private static final long serialVersionUID = -4752542581534740735L;
	Logger logger = LogManager.getLogger(this.getClass());
	Map<String,Object> request = null;
	Map<String,Object> session = null;

	public String execute() {
		logger.debug("Executing ShowBooksCommand");
		BookManagerServiceHelper helper = new BookManagerServiceHelper();
		UserManagerServiceHelper usersHelper = new UserManagerServiceHelper();
		LoginInfo login = (LoginInfo) session.get("loginInfo");
		
		try {
			request.put("books", helper.getBooks());
			request.put("user", usersHelper.getUser(login.getLogin()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;	
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	
}
