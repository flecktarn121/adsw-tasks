package com.miw.presentation.actions;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.ShoppingCart;
import com.miw.presentation.book.BookManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@Result(name="success", location="view-shopping-cart.action", type = "redirectAction")
public class AddToShoppingCartAction extends ActionSupport implements SessionAware{

	private static final long serialVersionUID = -5635863473204400635L;
	private Map<String, Object> session = null;
	private List<String> addedBooks = null;

	@Override
	public String execute() throws Exception {
		ShoppingCart cart = getShoppingCart();

		for (String bookId : addedBooks) {
			BookManagerServiceHelper helper = new BookManagerServiceHelper();
			cart.Add(helper.getBook(Integer.parseInt(bookId)));
		}

		return SUCCESS;
	}

	public ShoppingCart getShoppingCart() {
		if (!this.session.containsKey("cart")) {
			this.session.put("cart", new ShoppingCart());
		}

		return (ShoppingCart) this.session.get("cart");
	}
	

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setAddedBooks(List<String> addedBooks) {
		this.addedBooks = addedBooks;
	}

}
