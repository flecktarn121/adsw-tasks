package com.miw.presentation.actions;

import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.ShoppingCart;
import com.miw.presentation.book.BookManagerServiceHelper;
import com.miw.presentation.sales.SalesManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@Results({ @Result(name = "success", location = "checkout-success.jsp"),
		@Result(name = "error", location = "checkout-error.jsp")

})
public class CheckoutAction extends ActionSupport implements SessionAware, RequestAware, ApplicationAware {
	private static final long serialVersionUID = 7130657255613308447L;
	private Map<String, Object> session = null;
	private Map<String, Object> request = null;
	private Map<String, Object> application = null;

	
	@Override
	public String execute() throws Exception {
		
		ShoppingCart cart = getShoppingCart();
		
		if (cart.getList().isEmpty()) {
			this.request.put("message", "There is nothing in your cart");
			return ERROR;
		}

		SalesManagerServiceHelper salesHelper = new SalesManagerServiceHelper();
		
		try {
			salesHelper.processCart(cart);
			session.put("cart", new ShoppingCart());
			application.put("booklist", (new BookManagerServiceHelper()).getBooks());
		} catch (IllegalArgumentException e) {
			this.request.put("message", "Not enough stock to fullfill order");
			return ERROR;
		}

		return SUCCESS;
	}
	
	public ShoppingCart getShoppingCart() {
		if (!this.session.containsKey("cart")) {
			this.session.put("cart", new ShoppingCart());
		}

		return (ShoppingCart) this.session.get("cart");
	}
	
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setApplication(Map<String, Object> application) {
		this.application = application;
	}

}
