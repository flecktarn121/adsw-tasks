package com.miw.presentation.actions;

import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

@Result(name="success", location="/index.jsp" )
public class SignoutAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = -1533398820866876507L;
	private Map<String, Object> session;
	
	@Override
	public String execute() throws Exception {
		if (session.containsKey("loginInfo")) {
			session.remove("loginInfo");
		}
		return SUCCESS;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
