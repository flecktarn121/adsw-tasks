package com.miw.model;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {

	private Map<String, BookCartItem> list;

	public ShoppingCart() {
		list = new HashMap<String, BookCartItem>();
	}

	public void Add(Book book) {
		String id = Integer.toString(book.getId());

		if (!list.containsKey(book.getId())) {
			list.put(id, new BookCartItem(book, 0));
		}

		int amount =  list.get(id).getAmount() + 1;
		list.get(id).setAmount(amount);
	}

	public Map<String, BookCartItem> getList() {
		return list;
	}

}
