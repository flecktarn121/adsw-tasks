package com.miw.model;

/**
 * Wrapper of the book class 
 * @author UO258654
 *
 */
public class BookCartItem {

	private Book book;
	private int amount;
	
	public BookCartItem() {
	}

	public BookCartItem(Book book, int amount) {
		this.book = book;
		this.amount = amount;
	}

	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
