
package com.miw.business.bookmanager;

import java.util.List;

import org.apache.logging.log4j.*;

import com.miw.business.BookDataServiceHelper;
import com.miw.model.Book;

public class BookManager implements BookManagerService {
	Logger logger = LogManager.getLogger(this.getClass());

	public List<Book> getBooks() throws Exception {
		logger.debug("Asking for books");
		List<Book> books = (new BookDataServiceHelper()).getBooks();

		// We calculate the final price with the VAT value
		for (Book b : books) {
			b.setPrice(b.getBasePrice() * (1 + b.getVat().getValue()));
			if (b.getStock() <= 3) {
				b.setPrice(b.getPrice() * 0.95);
			}
		}
		return books;
	}

	public Book getSpecialOffer() throws Exception {
		List<Book> books = getBooks();
		int number = (int) (Math.random() * books.size());
		logger.debug("Applying disccount to " + books.get(number).getTitle());
		books.get(number).setPrice((double) books.get(number).getPrice() * 0.85);
		return books.get(number);
	}

	public void addBook(Book book) throws Exception {
		(new BookDataServiceHelper()).addBook(book);
	}

	public Book getBook(int bookId) throws Exception {
		logger.debug("Asking for a book");
		Book book = (new BookDataServiceHelper()).getBook(bookId);

		// We calculate the final price with the VAT value
		book.setPrice(book.getBasePrice() * (1 + book.getVat().getValue()));
		if (book.getStock() <= 3) {
			book.setPrice(book.getPrice() * 0.95);
		}
		return book;
	}
}
