package com.miw.business.vatmanager;

import java.util.List;

import com.miw.model.VAT;

public interface VATManagerService {

	public List<VAT> getVATs() throws Exception;
}
