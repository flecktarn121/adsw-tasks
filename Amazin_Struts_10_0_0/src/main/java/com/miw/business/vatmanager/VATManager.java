package com.miw.business.vatmanager;

import java.util.List;

import com.miw.business.VATDataServiceHelper;
import com.miw.model.VAT;

public class VATManager implements VATManagerService {

	public List<VAT> getVATs() throws Exception {
		return (new VATDataServiceHelper()).getVATs();
	}

}
