package com.miw.business.usermanager;

import com.miw.business.UsersDataServiceHelper;
import com.miw.business.exception.AlreadyRegisteredUserException;
import com.miw.model.User;

public class UserManager implements UserManagerService {

	public User getUser(String username) throws Exception {
		UsersDataServiceHelper helper = new UsersDataServiceHelper();

		User user = helper.getUser(username);
		return user;
	}

	public void addUser(User user) throws Exception {
		UsersDataServiceHelper helper = new UsersDataServiceHelper();

		if (helper.existsUsername(user.getUsername())) {
			throw new AlreadyRegisteredUserException();
		}

		helper.addUser(user);
	}

	public boolean existsUser(String username) throws Exception {
		UsersDataServiceHelper helper = new UsersDataServiceHelper();

		return helper.existsUsername(username);

	}
}
