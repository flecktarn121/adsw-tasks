package com.miw.business.usermanager;

import com.miw.model.User;

public interface UserManagerService {

	public User getUser(String username) throws Exception;
	
	public void addUser(User user) throws Exception;
	
	public boolean existsUser(String username) throws Exception;
}
