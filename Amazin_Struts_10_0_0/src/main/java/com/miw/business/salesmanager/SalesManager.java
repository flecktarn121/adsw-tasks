package com.miw.business.salesmanager;

import com.miw.business.BookDataServiceHelper;
import com.miw.model.Book;
import com.miw.model.ShoppingCart;

public class SalesManager implements SalesManagerService {

	public synchronized void processCart(ShoppingCart cart) throws Exception {
		BookDataServiceHelper helper = new BookDataServiceHelper();

		for(String bookId: cart.getList().keySet()) {
			Book book = helper.getBook(Integer.parseInt(bookId));
			int quantity = cart.getList().get(bookId).getAmount();

			if (quantity > book.getStock()) {
				throw new IllegalArgumentException("Not enough stock for book " + bookId);
			}
			
			book.setStock(book.getStock() - quantity);
			
			helper.updateBook(book);
		}
	}

}
