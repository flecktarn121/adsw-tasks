package com.miw.business.salesmanager;

import com.miw.model.ShoppingCart;

public interface SalesManagerService {
	
	public void processCart(ShoppingCart cart) throws Exception;

}
