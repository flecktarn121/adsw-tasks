package com.miw.business;

import com.miw.infrastructure.Factories;
import com.miw.model.User;

public class UsersDataServiceHelper {

	public User getUser(String username) throws Exception {
		return (Factories.dataServices.getUserDataService()).getUser(username);
	}
	
	public void addUser(User user) throws Exception {
		(Factories.dataServices.getUserDataService()).addUser(user);
	}

	public boolean existsUsername(String username) throws Exception {
		return (Factories.dataServices.getUserDataService()).existsUsername(username);
	}
}
