package com.miw.business;

import com.miw.business.bookmanager.BookManagerService;
import com.miw.business.salesmanager.SalesManagerService;
import com.miw.business.usermanager.UserManagerService;
import com.miw.business.vatmanager.VATManagerService;

public interface ServicesFactory {
	public BookManagerService getBookManagerService();

	public SalesManagerService getSalesManagerService();

	public UserManagerService getUserManagerService();

	public VATManagerService getVATManagerService();
	
}
