package com.miw.business;

import org.apache.logging.log4j.*;

import com.miw.business.bookmanager.BookManager;
import com.miw.business.bookmanager.BookManagerService;
import com.miw.business.salesmanager.SalesManager;
import com.miw.business.salesmanager.SalesManagerService;
import com.miw.business.usermanager.UserManager;
import com.miw.business.usermanager.UserManagerService;
import com.miw.business.vatmanager.VATManager;
import com.miw.business.vatmanager.VATManagerService;

public class SimpleServicesFactory implements ServicesFactory {
	
	Logger logger = LogManager.getLogger(this.getClass());

	public BookManagerService getBookManagerService() {
		logger.debug("Serving an instance of BookManagerService from "+this.getClass().getName());
		return new BookManager();
	}

	public SalesManagerService getSalesManagerService() {
		logger.debug("Serving an instance of SalesManagerService from "+this.getClass().getName());
		return new SalesManager();
	}

	public UserManagerService getUserManagerService() {
		logger.debug("Serving an instance of UserManagerService from "+this.getClass().getName());
		return new UserManager();
	}

	public VATManagerService getVATManagerService() {
		// TODO Auto-generated method stub
		logger.debug("Serving an instance of VATManagerService from "+this.getClass().getName());
		return new VATManager();
	}

}
