package com.miw.persistence.user;

import com.miw.model.User;

public interface UserDataService {

	public User getUser(String username) throws Exception;
	
	public void addUser(User user) throws Exception;
	
	public boolean existsUsername(String username) throws Exception;

}
