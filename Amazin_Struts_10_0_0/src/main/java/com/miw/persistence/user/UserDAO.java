package com.miw.persistence.user;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.*;
import com.miw.model.User;
import com.miw.persistence.Dba;

public class UserDAO implements UserDataService {

	protected Logger logger = LogManager.getLogger(getClass());

	public User getUser(String username) throws Exception {
		User result = null;

		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			result = em.createQuery("Select a From User a where a.username = :username", User.class)
					.setParameter("username", username)
					.getSingleResult();

			logger.debug("Result user: "+ result.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return result;
	}

	public void addUser(User user) throws Exception {
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			em.persist(user);

			logger.debug("Inserting user: "+ user.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}
	}
	
	public boolean existsUsername(String userName) throws Exception{
			Dba dba = new Dba();
		try {
			logger.debug("Checking if user already exists: "+ userName);
			EntityManager em = dba.getActiveEm();

			long result = (Long) em.createQuery("SELECT COUNT(*) FROM User WHERE username = :username")
				.setParameter("username", userName)
				.getSingleResult();

			return result != 0;

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}	
	}

}
