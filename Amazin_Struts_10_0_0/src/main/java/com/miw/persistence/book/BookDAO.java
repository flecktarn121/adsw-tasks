package com.miw.persistence.book;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.*;

import com.miw.model.Book;
import com.miw.persistence.Dba;

public class BookDAO implements BookDataService  {

	protected Logger logger = LogManager.getLogger(getClass());

	public List<Book> getBooks() throws Exception {

		List<Book> resultList = null;

		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();
			
			resultList = em.createQuery("Select a From Book a", Book.class).getResultList();

			logger.debug("Result list: "+ resultList.toString());
			for (Book next : resultList) {
				logger.debug("next book: " + next);
			}

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return resultList;
	}
	
	public Book getBook(int id) {
		Book result = null;

		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			result = em.createQuery("Select a From Book a where a.id = :id", Book.class)
					.setParameter("id", id)
					.getSingleResult();

			logger.debug("Result book: "+ result.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return result;
	}
	
	public void updateBook(Book book) {
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			em.createQuery("Update Book a  set a.stock = :stock where a.id = :id")
				.setParameter("stock", book.getStock())
				.setParameter("id", book.getId())
				.executeUpdate();

			logger.debug("Updating book: "+ book.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}
	}
	
	public void addBook(Book book) {
			Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			em.persist(book);

			logger.debug("Adding book: "+ book.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}	
	}
}