<!DOCTYPE html >
<%@ page contentType="text/html; charset=iso-8859-1"
	pageEncoding="iso-8859-1" language="java"
	import="java.util.*, com.miw.model.Book,com.miw.presentation.book.*"
	errorPage=""%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page isELIgnored="false"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<head>
<title>Amazin</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header">Amazin.com</h1>
		<h2 class="centered">
			Welcome to the <em>smallest</em> online shop in the world!!
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.action">Start</a></li>
		<li><a href="view-shopping-cart.action">Cart</a></li>
			<li><a href="signout.action">Sign out</a></li>
			<li><a href="http://miw.uniovi.es">About</a></li>
			<li><a href="mailto:dd@email.com">Contact</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<table>
				<caption>Our catalog:</caption>
				<thead>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Description</th>
						<th>Price</th>
						<th>Stock</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="#request.books" var="book">
						<tr>
							<td>
							<s:property value="#book.title"/>
							<s:if test="#book.stock <= 3">
								<strong>(Last units!)</strong>
							</s:if>
							</td>
							<td><s:property value="#book.author" /></td>
							<td><s:property value="#book.description" /></td>
							<td><s:property value="#book.price" /> &euro;</td>
							<td><s:property value="#book.stock" /></td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			<p><a href="add-to-shopping-cart-form.action">Add to shopping cart</a> </p>
			<s:if test="#request.user.isAdmin">
				<p><a href="add-book-form.action">Add books to the shop</a> </p>	
			</s:if>
		</article>
	</section>
	<footer>
		<strong> Master in Web Engineering (miw.uniovi.es).</strong><br /> <em>University
			of Oviedo </em>
	</footer>
</body>