<!DOCTYPE html >
<%@ page contentType="text/html; charset=iso-8859-1"
	pageEncoding="iso-8859-1" language="java"
	import="java.util.*, com.miw.model.Book,com.miw.presentation.book.*"
	errorPage=""%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page isELIgnored="false"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<head>
<title>Amazin</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header">Amazin.com</h1>
		<h2 class="centered">
			Welcome to the <em>smallest</em> online shop in the world!!
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.action">Start</a></li>
			<li><a href="view-shopping-cart.action">Cart</a></li>
			<li><a href="signout.action">Sign out</a></li>
			<li><a href="http://miw.uniovi.es">About</a></li>
			<li><a href="mailto:dd@email.com">Contact</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<table>
				<caption>Your shopping cart:</caption>
				<thead>
					<tr>
						<th>Id</th>
						<th>Ammount</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="%{#session.cart.list}">
						<tr>
							<td><s:property value="value.book.title" /></td>
							<td><s:property value="value.amount" /></td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			<p><strong>Total: <s:property value="#request.totalAmount"/> $</strong></p>
			<p>
			<a href="checkout.action">Checkout</a>
			</p>
		</article>
	</section>
	<footer>
		<strong> Master in Web Engineering (miw.uniovi.es).</strong><br /> <em>University
			of Oviedo </em>
	</footer>
</body>