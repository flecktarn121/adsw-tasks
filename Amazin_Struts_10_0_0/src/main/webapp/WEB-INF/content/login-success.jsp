<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title>Amazin</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header">Amazin.com</h1>
		<h2 class="centered">
			Welcome to the <em>smallest</em> online shop in the world!!
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="#">Start</a></li>
			<li><a href="view-shopping-cart.action">Cart</a></li>
			<li><a href="signout.action">Sign out</a></li>
			<li><a href="http://miw.uniovi.es">About</a></li>
			<li><a href="mailto:dd@email.com">Contact</a></li>
		</ul>
	</nav>
	<section>
		<article id="a01">
			<h2 class="mytitle">Choose an option:</h2>
			<p><a href="show-books.action">Show Catalog</a> </p>
			<p><a href="show-special-offer.action">Show Special Offers!</a> </p>	
		</article>
	</section>
	<footer>
		<strong> Master in Web Engineering (miw.uniovi.es).</strong><br /> <em>University
			of Oviedo </em><br/>
			Visits:	<s:property value="%{#application.counter}" />
			
	</footer>
</body>
