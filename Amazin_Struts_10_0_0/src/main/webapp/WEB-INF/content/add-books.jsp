<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title>Amazin</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header">Amazin.com</h1>
		<h2 class="centered">
			Welcome to the <em>smallest</em> online shop in the world!!
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.action">Start</a></li>
			<li><a href="view-shopping-cart.action">Cart</a></li>
			<li><a href="signout.action">Sign out</a></li>
			<li><a href="http://miw.uniovi.es">About</a></li>
			<li><a href="mailto:dd@email.com">Contact</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<h2 class="mytitle">Add books to the store</h2><br />
			<div style="color: red;">
			<p>
				<s:property value="#request.mymessage" />
			</p>
			</div>
			<s:form action="add-book"  >
				<s:textfield name="book.title" label="Title" />
				<s:textarea name="book.description" label="Description" />
				<s:textfield name="book.author" label="Author" />
				<s:textfield name="stockText" type="number" label="Initial stock" />
				<s:textfield name="basePriceText" label="Base price" />
				<s:select name="vatText" list="{'16%', '7%'}" label="VAT"></s:select>
				<s:submit />
			</s:form>
		</article>
	</section>
	<footer>

		<strong> Master in Web Engineering (miw.uniovi.es).</strong><br /> <em>University
			of Oviedo </em>
	</footer>
</body>
