package com.miw.model;



import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class LoginData {

	@NotNull
	@Length(min = 8, max = 64, message = "Debe tener entre 8 y 64 caracteres.")
	private String login;
	@NotNull
	@Length(min = 1, max = 8, message = "Debe tener entre 1 y 8 caracteres.")
	private String password;
	private String captcha;
	
	public String getCaptcha() {
		return captcha;
	}
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	@Override
	public String toString() {
		return "LoginData [login=" + login + ", password=" + password + "]";
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}

