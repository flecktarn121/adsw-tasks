package com.miw.model;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {
	private Map<String, BookCartItem> list;

	public ShoppingCart() {
		list = new HashMap<String, BookCartItem>();
	}

	public void add(Book book, int amount) {
		String id = Integer.toString(book.getId());

		if (!list.containsKey(id)) {
			list.put(id, new BookCartItem(book, 0));
		}

		int newAmount = list.get(id).getAmount() + amount;
		list.get(id).setAmount(newAmount);
	}

	public Map<String, BookCartItem> getList() {
		return list;
	}

	public double getTotalAmount() {
		double totalAmount = 0;

		for (BookCartItem item : list.values()) {
			totalAmount += (item.getAmount() * item.getBook().getPrice());
		}

		return totalAmount;
	}

}
