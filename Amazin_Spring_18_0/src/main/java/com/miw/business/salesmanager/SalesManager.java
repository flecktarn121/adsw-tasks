package com.miw.business.salesmanager;

import com.miw.model.Book;
import com.miw.model.ShoppingCart;
import com.miw.persistence.book.BookDataService;

public class SalesManager implements SalesManagerService {

	private BookDataService bookDataService= null;

	public BookDataService getBookDataService() {
		return bookDataService;
	}

	public void setBookDataService(BookDataService bookDataService) {
		this.bookDataService = bookDataService;
	}

	public synchronized void processCart(ShoppingCart cart) throws Exception {

		for (String bookId : cart.getList().keySet()) {
			Book book = bookDataService.getBook(Integer.parseInt(bookId));
			int quantity = cart.getList().get(bookId).getAmount();

			if (quantity > book.getStock()) {
				throw new IllegalArgumentException("Not enough stock for book " + bookId);
			}

			book.setStock(book.getStock() - quantity);

			bookDataService.updateBook(book);
		}
	}

}
