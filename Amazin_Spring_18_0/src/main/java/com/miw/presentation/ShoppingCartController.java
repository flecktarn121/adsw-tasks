package com.miw.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.bookmanager.BookManagerService;
import com.miw.model.Book;
import com.miw.model.ShoppingCart;
import com.miw.presentation.cart.CartItem;

@Controller
@RequestMapping("private/shoppingCart")
public class ShoppingCartController {

	@Autowired
	private BookManagerService bookManager;

	@RequestMapping(method = RequestMethod.GET)
	public String seekBooks(Model model, HttpSession session) throws Exception {
		System.out.println("Executing SeekBooks.");

		// We store the list of books in teh model.
		model.addAttribute("booklist", bookManager.getBooks());

		List<Integer> amountList = new ArrayList<Integer>();
		for (int i = 1; i <= 10; i++) {
			amountList.add(i);
		}
		model.addAttribute("amountList", amountList);

		if (session.getAttribute("cart") == null) {
			session.setAttribute("cart", new ShoppingCart());
		}

		ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
		model.addAttribute("cart", cart.getList());
		model.addAttribute("totalAmount", cart.getTotalAmount());

		// We return the name of the view.
		return "private/shoppingCart";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid @ModelAttribute CartItem item, BindingResult result, Model model, HttpSession session)
			throws Exception {
		if (session.getAttribute("cart") == null) {
			session.setAttribute("cart", new ShoppingCart());
		}
		ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
		Book book = bookManager.getBook(item.getBookId());
		cart.add(book, item.getAmount());

		return "redirect:shoppingCart";
	}

	@ModelAttribute
	CartItem getCartItem() {
		return new CartItem();
	}

}
