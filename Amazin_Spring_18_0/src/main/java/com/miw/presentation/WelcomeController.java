package com.miw.presentation;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

	/*
	 * Setting / as request mapping url we are setting the default controller for the application.
	 */
	@RequestMapping("/")
	public String welcome()
	{
		System.out.println("Executing Welcome controller");
		return "redirect:/private/menu";
	}
	@RequestMapping("/private/menu")
	public String index(Principal p) {
		System.out.println("Executing private/menu controller for user "+ p);
		return "private/index";
	}
	@RequestMapping("loginError")
	public String loginError(ModelMap model)
	{
		model.addAttribute("error","true");
		model.addAttribute("message", "Validation error");
		return "login";
	}
	
	@RequestMapping("login")
	public String getForm() {
		System.out.println("Preparing the model for Login");
		return "login";
	}	
	
	@RequestMapping("error")
	public String error() {
		return "error";
	}	
}
