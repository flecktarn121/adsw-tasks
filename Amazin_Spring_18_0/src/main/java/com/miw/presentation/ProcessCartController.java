package com.miw.presentation;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.salesmanager.SalesManagerService;
import com.miw.model.ShoppingCart;

@Controller
@RequestMapping("private/processCart")
public class ProcessCartController {

	@Autowired
	private SalesManagerService salesManager;

	@RequestMapping(method = RequestMethod.GET)
	public void seekBooks(Model model, HttpSession session) throws Exception {
		System.out.println("Executing Process Cart.");

		if (session.getAttribute("cart") == null) {
			session.setAttribute("cart", new ShoppingCart());
		}

		ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
		model.addAttribute("cart", cart.getList());
		model.addAttribute("totalAmount", cart.getTotalAmount());
	}

	@RequestMapping(method = RequestMethod.POST)
	public String post(Model model, HttpSession session)
			throws Exception {
		if (session.getAttribute("cart") == null) {
			session.setAttribute("cart", new ShoppingCart());
		}

		ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
		try {
		salesManager.processCart(cart);
		session.removeAttribute("cart");
		} catch (IllegalArgumentException e) {
			model.addAttribute("meesage", "Not enough stock to fulfill your order. :(");
			model.addAttribute("totalAmount", cart.getTotalAmount());
			return "private/processCart";
		}

		return "private/success";
	}

	public SalesManagerService getSalesManager() {
		return salesManager;
	}

	public void setSalesManager(SalesManagerService salesManager) {
		this.salesManager = salesManager;
	}
}
