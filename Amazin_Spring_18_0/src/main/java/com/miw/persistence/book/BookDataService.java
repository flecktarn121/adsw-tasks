package com.miw.persistence.book;

import java.util.List;

import com.miw.model.Book;

public interface BookDataService {

	List<Book> getBooks() throws Exception;
	public Book newBook(Book book) throws Exception;
	public Book getBook(int bookId) throws Exception;
	public void updateBook(Book book) throws Exception;
}