<!DOCTYPE html >
<%@ page contentType="text/html; charset=iso-8859-1"
	pageEncoding="iso-8859-1" language="java"
	import="java.util.*, com.miw.model.Book,com.miw.presentation.book.*"
	errorPage=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page isELIgnored="false"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<head>
<title>Amazin</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
	<header>
		<h1 class="header">Amazin.com</h1>
		<h2 class="centered">
			Welcome to the <em>smallest</em> online shop in the world!!
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="/Amazin/">Start</a></li>
			<li><a href="newBook">Add New</a></li>
			<li><a href="http://miw.uniovi.es">About</a></li>
			<li><a href="mailto:dd@email.com">Contact</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<table>
				<caption>Our catalog:</caption>
				<thead>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Description</th>
						<th>Price</th>
						<th>Stock</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var='book' items="${booklist}">
						<tr>
							<td><c:out value="${book.title}" /> <c:if
									test="${book.stock <= 3 }">
									<strong> (Last units!)</strong>
								</c:if></td>
							<td><c:out value="${book.author}" /></td>
							<td><c:out value="${book.description}" /></td>
							<td><c:out value="${book.price}" /> &euro;</td>
							<td><c:out value="${book.stock}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<p>
				<a href="shoppingCart">Add books to cart</a><br />
			</p>
		</article>
	</section>
	<footer>
		<strong> Master in Web Engineering (miw.uniovi.es).</strong><br /> <em>University
			of Oviedo </em>
	</footer>
</body>