<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title>Amazin</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
	<header>
		<h1 class="header">Amazin.com<spring:message code="title"/></h1>
		<h2 class="centered">
			Welcome to the <em>smallest</em> online shop in the world!!<spring:message code="welcome"/>
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="#">Start</a></li>
			<li><a href="http://miw.uniovi.es">About</a></li>
			<li><a href="mailto:dd@email.com">Contact</a></li>
		</ul>
	</nav>
	<section>
		<article id="a01">
			<h3 class="mytitle">Choose an option:<spring:message code="choose.option"/></h3>
			<p><a href="showBooks">Show Catalog<spring:message code="show.catalog"/></a></p>
			<p><a href="showSpecialOffer">Show Special Offers!<spring:message code="show.offers"/></a> </p>
			<p><a href="newBook">Add new book to the store</a> </p>
		</article>
	</section>
	<footer>
		<strong> Master in Web Engineering (miw.uniovi.es).</strong><br /> <em>University
			of Oviedo </em>
	</footer>
</body>
